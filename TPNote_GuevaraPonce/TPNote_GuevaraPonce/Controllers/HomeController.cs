﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPNote_GuevaraPonce.Models;

namespace TPNote_GuevaraPonce.Controllers
{
    public class HomeController : Controller
    {
        List<Produit> produits = new List<Produit>();
        public ActionResult Index(string commande, string query)
        {
            if (string.IsNullOrWhiteSpace(commande))
            {               
                return View("Menu");
            }
            produits = populerProduits();
           
            switch (commande)
            {
                case "alpha":
                    return View("Alpha", produits.OrderBy(x => x.LeNom));
                case "query":
                    if (string.IsNullOrWhiteSpace(query))
                    {
                        return View("Menu");
                    }
                    switch (query.ToUpper())
                    {
                        case "JEU":
                            return View("Query",produits.Where(x=> x.LeType == 1));
                        case "FIGURINE":
                            return View("Query", produits.Where(x => x.LeType == 2));
                        case "POSTER":
                            return View("Query", produits.Where(x => x.LeType == 3));
                        
                                                       

                    }
                    return View("MENU");
                case "decr":
                    return View("Decr", produits.OrderByDescending(x => x.LePrix));
                case "search":
                    if (string.IsNullOrWhiteSpace(query))
                    {
                        return View("Menu");
                    }
                    return View("Search", produits.FindLast(x => x.LeNom == query));
            }
            return View("Menu");
        }

      

        private List<Produit> populerProduits()
        {
            List<Produit> newproduits = new List<Produit>();
            newproduits.Add(new Produit("allo", 1, 10.0));
            newproduits.Add(new Produit("test2", 3, 200.0));
            newproduits.Add(new Produit("test3",1, 3.0));
            newproduits.Add(new Produit("zest4", 2, 2.5));
            newproduits.Add(new Produit("test5", 3, 3.9));
            newproduits.Add(new Produit("test7", 1, 139.0));
            return newproduits;
        }

    }
}